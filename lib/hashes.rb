# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_length = {}
  str.split(" ").each do |word|
    word_length[word] = word.length
  end
  return word_length
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  ordered_hash = hash.sort_by { |k, v| v }
  ordered_hash[-1].first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |item, stock|
    older[item] = stock
  end
  return older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  count = Hash.new(0)
  word.split("").each { |k, v| count[k] += 1 }
  return count
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  unique_hash = Hash.new(0)
  arr.each do |number|
    unique_hash[number] += 1
  end
  return unique_hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  numbers_hash = {even: 0, odd: 0}
  numbers.each do |num|
    if num.even?
      numbers_hash[:even] += 1
    else
      numbers_hash[:odd] += 1
    end
  end
  return numbers_hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowel_count = Hash.new(0)
  string.chars.each do |char|
    vowel_count[char] += 1
  end
  vowel_array = vowel_count.sort_by {|letter, count| count}
  return vowel_array[-1].first
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]

def fall_and_winter_birthdays(students)
  second_half_birthdays = students.select { |student, month| month >= 7 }

  student_names = second_half_birthdays.keys
  student_pairs = []

  for i in 0...student_names.length
    for j in i+1...student_names.length
      student_pairs << [student_names[i], student_names[j]]
    end
  end
  return student_pairs
end


# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  biodiversity_hash = Hash.new(0)
  specimens.each do |animal|
    biodiversity_hash[animal] += 1
  end
  sorted_biodiversity = biodiversity_hash.sort_by {|specimen, number| number}
  return biodiversity_hash.length ** 2 * sorted_biodiversity[0].last / sorted_biodiversity[-1].last
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_count = character_count(normal_sign)
  vandalized_count = character_count(vandalized_sign)

  vandalized_count.all? do |char, count|
    normal_count[char] >= count
  end
end

def character_count(str)
  count = Hash.new(0)
  str.split("").each do |char|
    count[char.downcase] += 1
  end
  return count
end
